### Prerequisites

* Basic knowledge of C Programming

### Course Content

#### Day 1

* C Build Process
* Linux Commands
* GCC & GDB
* Makefiles

#### Day 2

* Preprocessor and its uses
* Pointers & Arrays
* Complex Strings Manipulations
* Structures & Unions
* Complicated Declarations
* Function Pointers

#### Day 3

* Data Structures and their applications
* Basic Networking using C
* Debugging

#### Day 4

* Coding Standards
* Writing an application
* Writing Makefile

#### Day 5

* Make a project of your choice